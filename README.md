# Start The Fire Lang

Esoteric language designed in the form of a verse of Billy Joel's hit song: "We Didn't Start The Fire"

Before beginning development, it is necessary to take note that Start The Fire Lang's variable storage is somewhat eccentric. Most programming languages rely on a reference based system, Starte the Fire Lang relies on a mention based system.

What this means:
In a reference based programming language, creating 2 integers, called billy, and joel, and adding three to billy would look like the following:
```
billy = 10
joel = 10
billy = billy+3
```
However, in a mention based language, it would look like this:
```
mention_int(billy)
//Because there is no variable called billy, it initializes one with the value of 0
add 10
//Adds 10 to the last mentioned int, in this case, setting billy to 10
mention_int(joel)
add 10
//In this case, it adds 10 to the recently initialized value of joel, setting it to 10
mention_int(billy)
add 3
//Adds 3 to the previously mentioned int, in this case, setting billy to 13
```
With that in mind, here's how the syntax of the language works
VARIABLE/VALUE, OPERATION

So the previous code would look like this, deobfusciated of course
```
billy, mention_int
10, add
joel, mention_int
10, add
billy, mention_int
```
*Note: Any line that does not contain a comma is treated as a comment. 

In the actual language, the lefthand is exactly as shown in the example, but the right-hand functions slightly differently. 
Instead of operators being defined by keywords, they are defined by _key word-endings_. It does not matter how the word begins, so long as it ends in one of the following word endings.
| Word Ending | Operator |
| ------ | ------ |
| ay | num_mention |
| it | string_mention |
| one | add |
| een | multiply |
| ace | exponent |
| ar | set_string |
| o | concatenate_string |
| ade | get_string_from_user |
| ate | get_num_from_user |
| ead | print |
| ake | goto |
If the word does not end in one of these values, it is treated as a comment

The same code as before, in runnable Start the Fire would look like this
```
Billy, Kitchen Tray
10, Electric Phone
Joel, March and May
10, All Alone
Billy, Dismay
3, Cone
```

Although Start the Fire is a mention based programming language, it still refers to variables by name when retriving their value.
To refer to a variable, simply reference it in text:
```
Billy, Kitchen Tray,
10, Electric Phone
Billy, Tone
```
This example creates a number called Billy, sets it to 10, and adds it to itself.
The program flow for getting what value works as follows:
```
Check if the num_mention cache has a value with the same name as the left hand side. If so, return the value.
Check if the string_mention cache has a value with the same name as the left hand side. If so, return the value.
Return the value as a string literal
```

Because this language is based on a song, it is strongly recommended to make the code rhyme, at the expense of performance.
Here's our example, adhering to this style:
```
Billy, Kitchen Tray
Mick Jagger, Michael Chey
10, I'm right now on the phone
Joel, I forgot to pay
NickToons, Carly Shay
10, My hat looks like a Cone
Billy, Chesapeake Bay
On a boat for the whole day
3, I don't like your Tone
And now Billy is all alone
```
