#pragma once
#include "ValueHolder.h";
#include "Command.h";
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
using namespace std;



class Interpreter {
	
public:
	
	void run() {
		for (int i = 0; i < commandStack.size(); i++) {
			Command c = commandStack[i];

			string value = c.leftHandValue;
			
			switch (c.command) {
			
			case CommandType::NUM_MENTION: {
				numHolder.mention(value);
				break;
			}

			case CommandType::STRING_MENTION: {
				stringHolder.mention(value);
				break;
			}
			
			case CommandType::ADD: {
				double num = numHolder.get();
				num += getNumValue(value);
				numHolder.set(num);
				break;
			}

			case CommandType::MULTIPLY: {
				double num = numHolder.get();
				num *= getNumValue(value);
				numHolder.set(num);
				break;
			}
			case CommandType::EXPONENT: {
				double num = numHolder.get();
				num = pow(num, getNumValue(value));
				numHolder.set(num);
				break;
			}
			case CommandType::STRING_SET: {
				string str = getStringValue(value);
				stringHolder.set(str);
				break;
			}
			case CommandType::CONCAT: {
				string str = stringHolder.get();
				str += getStringValue(value);
				stringHolder.set(str);
				break;
			}
			
			case CommandType::GET_STRING:{
				string str = getStringValue(value);
				cout << str;
				string in;
				cin >> in;
				stringHolder.set(in);
				break;
			}

			case CommandType::GET_NUM: {
				string str = getStringValue(value);
				cout << str;
				float in;
				cin >> in;
				numHolder.set(in);
				break;
			}

			case CommandType::PRINT: {
				cout << getStringValue(value) << endl;
				break;
			}

			case CommandType::GOTO: {
				int num = (int)getNumValue(value);
				i = num - 2;
				break;
			}

			}
		}
	}

	void addCommand(Command c) {
		commandStack.push_back(c);
	}

	Command interpretLine(string line) {
		Command out;

		int chari = line.find(',');
		if (chari == -1) {
			out.command = CommandType::NOTHING;
			return out;
		}

		string name = line.substr(0, chari);
		string opWord = line.substr(chari);

		out.leftHandValue = name;
		out.command = CommandType::NOTHING;
		map<string, CommandType>::iterator i;
		for (i = commands.begin(); i != commands.end(); i++) {
			string type = i->first;
			CommandType com = i->second;
			if (isLast(opWord, type)) {
				out.command = com;
				break;
			}
		}
	
		return out;
	}



protected:
	map<string, CommandType> commands = {
		{"ay", CommandType::NUM_MENTION},
		{"it", CommandType::STRING_MENTION},
		
		{"one", CommandType::ADD},
		{"een", CommandType::MULTIPLY},
		{"ace", CommandType::EXPONENT},

		{"ar", CommandType::STRING_SET},
		{"o", CommandType::CONCAT},

		{"ade", CommandType::GET_STRING},
		{"ate", CommandType::GET_NUM},
		{"ead", CommandType::PRINT},
		
		{"ake", CommandType::GOTO}
	};
	ValueHolder<string> stringHolder;
	ValueHolder<double> numHolder;
	vector<Command> commandStack;

private:
	static string toLower(string s) {
		string allLower;
		for (char c : s) {
			allLower += (char)tolower(c);
		}
		return allLower;
	}
	static bool isLast(string s, string end) {
		return s.substr(s.length() - end.length()) == end;

	}
	static string removeWhitespace(string s) {
		const string whiteSpace = ", \t";
		int begin = s.find_first_not_of(whiteSpace);
		int end = s.find_last_not_of(whiteSpace);
		return s.substr(begin, end - begin + 1);
	}

	string getStringValue(string ref) {
		//Amiguous int float string parsing
		if (numHolder.hasKey(ref)) {
			string str =  to_string(numHolder.getValue(ref));
			int decimalIndex = str.find('.');
			if (decimalIndex == -1) {
				return str;
			}
			int removeZero = str.find_last_not_of('0');
			str = str.substr(0,removeZero+1);
			if (decimalIndex == str.length() - 1) {
				return str.substr(0, str.length() - 1);
			}
			return str;
		}
		else if (stringHolder.hasKey(ref)) {
			return stringHolder.getValue(ref);
		}
		else {
			return ref;
		}
	}

	double getNumValue(string ref) {
		if (numHolder.hasKey(ref)) {
			return numHolder.getValue(ref);
		}
		else {
			try {
				double num = stof(ref);
				return num;
			}
			catch (exception e) {
				return 0;
			}
		}
	}
};