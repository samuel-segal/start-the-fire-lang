#pragma once

#include <string>
#include <map>
using namespace std;

template <typename T> class ValueHolder {
public:
	void mention(string name) {
		if (values.find(name) == values.end()) {
			values.insert(values.begin(), pair<string, T>(name, getDefaultValue()));
		}
		mainName = name;
		
	}
	T get() {
		return values[mainName];
	}
	void set(T t) {
		values[mainName] = t;
	}
	T getValue(string key) {
		return values[key];
	}
	void setValue(string key, string value) {
		values[key] = value;
	}
	bool hasKey(string key) {
		return values.find(key) != values.end();
	}

protected:
	string mainName;
	map<string, T> values;
	T getDefaultValue() {
		return T();
	}
};