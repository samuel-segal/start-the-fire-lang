#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include "ValueHolder.h"
#include "Interpreter.h"
#include "Command.h"
using namespace std;




Interpreter interpreter;

void runInterpret() {
	ifstream basefile("run.wdstf");
	
	string line;
	while (getline(basefile,line)) {
		Command c = interpreter.interpretLine(line);
		interpreter.addCommand(c);
	}

}

int main()
{
	runInterpret();
	cout << "Program ready" << endl;
	interpreter.run();
	
}
