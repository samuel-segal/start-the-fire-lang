#pragma once
#include <string>

enum class CommandType {
	NOTHING,
	NUM_MENTION,
	STRING_MENTION,
	ADD,
	MULTIPLY,
	EXPONENT,
	STRING_SET,
	CONCAT,
	PRINT,
	GET_STRING,
	GET_NUM,
	GOTO
};

struct Command {
	CommandType command;
	std::string leftHandValue;
};